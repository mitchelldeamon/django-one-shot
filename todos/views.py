from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoListForm


# Create your views here.
def show_todolist(request):
    get_todolist = TodoList.objects.all()

    context = {
        "list_objects": get_todolist,
    }

    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    get_details = get_object_or_404(TodoList, id=id)
    context = {"get_details": get_details}

    return render(request, "todos/details.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            form_save = form.save()
            return redirect("todo_list_detail", id=form_save.id)
    else:
        form = TodoListForm()

    context = {
        "form": form,
    }

    return render(request, "todos/create.html", context)


def todo_list_update(request, id):
    update_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=update_list)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListForm(instance=update_list)

    context = {
        # "update": update_list,
        "form": form,
    }

    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    delete_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        delete_list.delete()
        return redirect(show_todolist)

    return render(request, "todos/delete.html")
