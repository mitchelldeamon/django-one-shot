# Generated by Django 4.1.7 on 2023-03-01 22:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("todos", "0002_todoitem"),
    ]

    operations = [
        migrations.AlterField(
            model_name="todoitem",
            name="todo_list",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="items",
                to="todos.todolist",
            ),
        ),
    ]
